import React from "react";
import "./App.css";
import Navbar from "./Components/Navbar";
import Balance from "./Components/Balance";
import ExpenseList from "./Components/ExpenseList";
import AddTransaction from "./Components/AddTransaction";
import SearchBox from "./Components/SearchBox";
import TransactionProvider from "./context/TransactionProvider";
import EditData from "./Components/EditData";

function App() {
  const [editing, setEditing] = React.useState(false);

  const [currentTran, setCurrentTran] = React.useState({
    id: null,
    order: "",
    date: "",
    concept: "",
    amount: null,
    type: "",
  });

  return (
    <TransactionProvider>
      <Navbar></Navbar>

      <div className="container">
        <div className="row justify-content-center">
          <div className="col text-center mt-3">
            <Balance></Balance>
          </div>
        </div>
        <hr></hr>
        <SearchBox></SearchBox>
        <hr></hr>
        <div className="row">
          <div className="col">
            <ExpenseList
              setCurrentTran={setCurrentTran}
              setEditing={setEditing}
            ></ExpenseList>
          </div>
        </div>

        <div className="row">
          <div className="col">
            {editing ? (
              <EditData
                currentTran={currentTran}
                setEditing={setEditing}
              ></EditData>
            ) : (
              <AddTransaction></AddTransaction>
            )}
          </div>
        </div>
      </div>
    </TransactionProvider>
  );
}

export default App;
