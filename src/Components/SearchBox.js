import React, { useState } from "react";
import { TransactionContext } from "../context/TransactionProvider";
import Pagination from "./Pagination";

const SearchBox = () => {
  const { setType, setFilter } = React.useContext(TransactionContext);

  return (
    <div>
      <h4 className="text-center">Search Box</h4>
      <form>
        <div className="row">
          <div className="col">
            <input
              type="text"
              className="form-control mb-2"
              placeholder="Search..."
              onChange={(e) => setFilter(e.target.value)}
            />
          </div>

          <p className="text-center font-weight-bold justify-content-center mt-2">
            By
          </p>

          <div className="col">
            <select
              id="inputState"
              className="form-control"
              onChange={(e) => setType(e.target.value)}
            >
              <option defaultValue="choose">Choose..</option>

              <option>type</option>
              <option>concept</option>
            </select>
          </div>
        </div>
      </form>
      {/* <div className="row">
        <div className="col">
          <Pagination></Pagination>
        </div>
      </div> */}
    </div>
  );
};

export default SearchBox;
