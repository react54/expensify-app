import React, { useState } from "react";

import { TransactionContext } from "../context/TransactionProvider";

const AddTransaction = () => {
  const { addTransaction, error } = React.useContext(TransactionContext);

  const [date, setDate] = useState("");
  const [concept, setConcept] = useState("");
  const [amount, setAmount] = useState("");
  const [type, setType] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    if (type === "expense") {
      addTransaction(date, concept, -amount, type);
      setDate(" ");
      setConcept(" ");
      setAmount("");
      setType("");
      return;
    } else if (type === "income") {
      addTransaction(date, concept, amount, type);
      setDate(" ");
      setConcept(" ");
      setAmount("");
      setType("");
      return;
    }
  };

  return (
    <div className="mt-5">
      <h4 className="text-center mb-4">Add Transaction</h4>
      <div className="row text-center mb-3">
        <div className="col">
          {error ? <span className="text-danger">{error}</span> : null}
        </div>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col">
            <input
              type="date"
              className="form-control mb-2"
              placeholder="Date"
              name="date"
              value={date}
              onChange={(e) => setDate(e.target.value)}
            />
          </div>
          <div className="col">
            <input
              type="text"
              className="form-control mb-2"
              placeholder="Concept"
              name="concept"
              value={concept}
              onChange={(e) => setConcept(e.target.value)}
            />
          </div>

          <div className="col">
            <input
              type="text"
              className="form-control mb-2"
              placeholder="Amount"
              name="amount"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>

          <div className="col">
            <select
              id="inputState"
              className="form-control"
              name="type"
              value={type}
              onChange={(e) => setType(e.target.value)}
            >
              <option defaultValue="Choose">Choose...</option>
              <option>expense</option>
              <option>income</option>
            </select>
          </div>
        </div>

        <div className="row justify-content-center">
          <div className="col-3 ">
            <button
              className="btn btn-dark btn-block mt-1"
              type="submit"
              value="addTransaction"
            >
              Add
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddTransaction;
