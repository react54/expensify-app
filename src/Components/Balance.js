import React from "react";
import { TransactionContext } from "../context/TransactionProvider";
import numeral from "numeral";

const Balance = () => {
  const { data } = React.useContext(TransactionContext);

  const amounts = data.map((tran) => parseInt(tran.amount));
  const total = amounts.reduce((acc, item) => (acc += item), 0);
  const totalFormated = numeral(total).format("$0,0.00");

  return (
    <div>
      <h1>Balance</h1>
      <h2 className={total < 2000 ? "expense" : "income"}>{totalFormated} </h2>
    </div>
  );
};

export default Balance;
