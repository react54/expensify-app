import React from "react";

const ModalEdit = () => {
  return (
    <div>
      <div className="modal" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Edit</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <input
                  type="date"
                  className="form-control mb-2"
                  placeholder="Date"
                  name="date"
                />

                <input
                  type="text"
                  className="form-control mb-2"
                  placeholder="Concept"
                  name="concept"
                />

                <input
                  type="text"
                  className="form-control mb-2"
                  placeholder="Amount"
                  name="amount"
                />

                <select id="inputState" className="form-control" name="type">
                  <option>Expense</option>
                  <option>Income</option>
                </select>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary">
                Save changes
              </button>
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalEdit;
