import React from "react";
import { TransactionContext } from "../context/TransactionProvider";

const Pagination = () => {
  const { data, postsPerPage, pagina } = React.useContext(TransactionContext);
  const pageNumbers = [];
  const totalPosts = data.length;
  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav>
      <ul className="pagination">
        {pageNumbers.map((number) => (
          <li className="page-item" key={number}>
            <a onClick={() => pagina(number)} className="page-link" href="!#">
              {number}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;
