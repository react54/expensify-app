import React from "react";
import { TransactionContext } from "../context/TransactionProvider";

import ExpenseItem from "./ExpenseItem";
import Pagination from "./Pagination";

const ExpenseList = ({ setCurrentTran, setEditing }) => {
  const { data } = React.useContext(TransactionContext);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Date</th>
            <th scope="col">Concept</th>
            <th scope="col">Amount</th>
            <th scope="col">Type(exp/inc)</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>

        {data.length === 0 ? (
          <div className="row justify-content-center">
            <div className="col text-center mt-5">
              <p>No expenses</p>
            </div>
          </div>
        ) : (
          <ExpenseItem
            setCurrentTran={setCurrentTran}
            setEditing={setEditing}
          ></ExpenseItem>
        )}
      </table>
    </div>
  );
};

export default ExpenseList;
