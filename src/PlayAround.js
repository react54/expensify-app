import React from "react";

const PlayAround = () => {
  // const abilities = pokemonInf.abilities
  //   .map((ability) => {
  //     return ability.ability.name
  //       .toLowerCase()
  //       .split("-")
  //       .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
  //       .join(" ");
  //   })
  //   .join(", ");

  const dataTran = [
    {
      order: 1,
      id: uuidv4(),
      date: moment().format("YYYY-MM-DD"),
      concept: "salary",
      type: "income",
      amount: +10000,
    },
    {
      order: 2,
      id: uuidv4(),
      date: moment().format("YYYY-MM-DD"),
      concept: "gasoline",
      type: "expense",
      amount: -500,
    },
    {
      order: 3,
      id: uuidv4(),
      date: moment().format("YYYY-MM-DD"),
      concept: "mom paycheck",
      type: "expense",
      amount: -800,
    },
    {
      order: 4,
      id: uuidv4(),
      date: moment().format("YYYY-MM-DD"),
      concept: "phone rent",
      type: "expense",
      amount: -200,
    },
  ];

  const [data, setData] = React.useState(dataTran);

  const updateUser = (data) => {
    editTransaction(
      data.id,
      data.order,
      data.date,
      data.concept,
      data.type,
      data.amount
    );
  };

  const editTransaction = (id, order, date, concept, type, amount) => {
    const findValue = data.map((tran) =>
      tran.id === id ? { order, date, concept, type, amount } : tran
    );

    setData(findValue);
  };

  const list = data.map((item) => {
    <li key={item.id}>
      {item.concept}-{item.amount}
    </li>;
  });

  return (
    <div>
      <h1>Play around</h1>
      {list}
      <form>
        <input type="text"></input>
        <input type="number"></input>
        <button></button>
      </form>
    </div>
  );
};

export default PlayAround;
