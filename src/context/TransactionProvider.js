import React from "react";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";

export const TransactionContext = React.createContext();

const TransactionProvider = (props) => {
  const dataTran = [
    {
      order: 1,
      id: uuidv4(),
      date: moment().format("DD-MM-YYYY"),
      concept: "salary",
      type: "income",
      amount: +10000,
    },
    {
      order: 2,
      id: uuidv4(),
      date: moment().format("DD-MM-YYYY"),
      concept: "gasoline",
      type: "expense",
      amount: -500,
    },
    {
      order: 3,
      id: uuidv4(),
      date: moment().format("DD-MM-YYYY"),
      concept: "mom paycheck",
      type: "expense",
      amount: -800,
    },
    {
      order: 4,
      id: uuidv4(),
      date: moment().format("DD-MM-YYYY"),
      concept: "phone rent",
      type: "expense",
      amount: -200,
    },
  ];

  const [data, setData] = React.useState(dataTran);

  const [error, setError] = React.useState(null);

  //Calculate the order

  const orderN = data.map((order) => {
    return order.order;
  });

  const oN = data.map((o) => o.order);

  /// Add Transaction

  const addTransaction = (date, concept, amount, type) => {
    if (!date.trim()) {
      setError("Date field is empty");
      return;
    } else if (!concept.trim()) {
      setError("Concept field  is empty");
      return;
    } else if (amount == null || amount == "") {
      setError("Amount field  is empty");
      return;
    } else if (!type.trim()) {
      setError("Type field  is empty");
      return;
    }
    setData([
      ...data,
      {
        order: orderN.length + 1,
        id: uuidv4(),
        date,
        concept,
        type,
        amount,
      },
    ]);
    setError(null);
  };

  /// Delete Transaction

  const deleteTransaction = (id) => {
    const fileteredTransaction = data.filter((tran) => tran.id !== id);
    setData(fileteredTransaction);
  };

  ///Edit Transaction

  const editTransaction = (id, order, date, concept, amount, type) => {
    const findValue = data.map((tran) =>
      tran.id === id ? { order, date, concept, type, amount } : tran
    );
    if (!date.trim()) {
      setError("Date field is empty");
      return;
    } else if (!concept.trim()) {
      setError("Concept field  is empty");
      return;
    } else if (amount == null || amount == "") {
      setError("Amount field  is empty");
      return;
    } else if (!type.trim()) {
      setError("Type field  is empty");
      return;
    }

    setData(findValue);
  };

  /// Search

  // const s = [{searched: searched, currentPosts: currentPosts}]

  const [filter, setFilter] = React.useState("");
  const [searched, setSearched] = React.useState([]);
  const [type, setType] = React.useState("");

  // /// Pagination

  // const [currentPage, setCurrentPage] = React.useState(1);
  // const [postsPerPage, setPostsPerPage] = React.useState(4);

  // // console.log(currentPage);

  // // // Paginate
  // // const indexOfLastPost = currentPage * postsPerPage;
  // // const indexOfFirstPost = indexOfLastPost - postsPerPage;
  // // const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);

  // // const [currentTran, setCurrentTran] = React.useState(currentPosts);

  // // const pagina = (pageNumber) => {
  // //   setCurrentPage(pageNumber);
  // // };
  // // console.log(currentTran);

  React.useEffect(() => {
    if (type === "type") {
      setSearched(
        data.filter((t) => {
          return t.type.toLowerCase().includes(filter.toLowerCase());
        })
      );
      return;
    } else if (type === "concept") {
      setSearched(
        data.filter((t) => {
          return t.concept.toLowerCase().includes(filter.toLowerCase());
        })
      );
      return;
    }

    setSearched(data);
  }, [filter, data, type]);

  return (
    <TransactionContext.Provider
      value={{
        data,
        setData,
        addTransaction,
        deleteTransaction,
        editTransaction,
        setSearched,
        filter,
        setFilter,
        searched,
        error,
        type,
        setType,
      }}
    >
      {props.children}
    </TransactionContext.Provider>
  );
};

export default TransactionProvider;
